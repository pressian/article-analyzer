import json
from urllib.parse import urlparse
from collections import Counter, OrderedDict

import requests
from newspaper import fulltext
from parsel import Selector

with open('data.json', 'r') as fp:
    data = json.load(fp)

def extract_site_from_url(url):
    hostname = urlparse(url).hostname.split('.')
    if hostname[-1] in ['com', 'net']:
        key = '.'.join(hostname[-2:])
    elif hostname[-1] == 'kr' and hostname[-2] in ['co']:
        key = '.'.join(hostname[-3:])
    else:
        key = None
    return key

phrases = list()
for article in data:
    url = article['canonicalurl'].strip()
    site = extract_site_from_url(url)
    text = fulltext(article['body'], language='ko')
    sel = Selector(text=article['body'])
    og = sel.xpath('//meta[starts-with(@property, "og:")]')
    r = requests.post('http://127.0.0.1:4567/normalize', data=text.encode('utf-8'))
    norm = r.json()['strings']
    r = requests.post('http://127.0.0.1:4567/extractPhrases', data=text.encode('utf-8'))
    data = r.json()
    for phrase in data['phrases']:
        phrases.append(phrase[:phrase.index("(")])

counted = Counter(phrases)
words = dict()
for k, v in counted.items():
    try:
        words[v].append(k)
    except KeyError:
        words[v] = [k,]

words = OrderedDict(sorted(words.items(), key=lambda x: x[0], reverse=True))
for k, v in words.items():
    for word in sorted(v):
        print(k, word)
