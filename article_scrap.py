import sys
import time
import json
import logging
import argparse
from datetime import datetime
from urllib.parse import urlparse

import redis
import requests
import w3lib.encoding
from parsel import Selector

logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(levelname)-8s %(message)s", level=logging.DEBUG)

# Some web servers blocked crawler such as Kyunghyang
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Safari/604.1.38',
}
query_canonurl = '//link[translate(@rel, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz")="canonical"]/@href'

def query_og(tag):
    return '//meta[translate(@property, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz")="og:{}"]/@content'.format(tag)

def extract_site_from_url(url):
    hostname = urlparse(url).hostname.split('.')
    if hostname[-1] in ['com', 'net']:
        key = '.'.join(hostname[-2:])
    elif hostname[-1] == 'kr' and hostname[-2] in ['co']:
        key = '.'.join(hostname[-3:])
    else:
        key = None
    return key

def fetch(url):
    redirected = url
    while True:
        r = requests.get(redirected.strip(), allow_redirects=False, headers=headers)
        if r.status_code >= 300 and r.status_code < 400:
            redirected = r.headers['location']
            continue
        else:
            break
    res = dict()
    res['url'] = url
    res['redirected_url'] = redirected
    sel = Selector(text=r.text)
    canonicalurl = sel.xpath(query_canonurl).extract_first() # Find canonical URL
    if canonicalurl == None:
        canonicalurl = sel.xpath(query_og('url')).extract_first() # Find Opengraph URL
    res['canonical_url'] = canonicalurl
    logging.debug(res)
    return res

def parse(url):
    site = extract_site_from_url(url)
    r = requests.get(url, allow_redirects=False, headers=headers)
    logging.debug(dict(r.headers)) # To serialize JSON format
    encoding, body = w3lib.encoding.html_to_unicode(None, r.content)
    res = dict()
    res['encoding'] = encoding
    sel = Selector(text=body)
    res['og_site_name'] = sel.xpath(query_og('site_name')).extract_first()
    res['og_type'] = sel.xpath(query_og('type')).extract_first()
    res['og_title'] = sel.xpath(query_og('title')).extract_first()
    res['og_description'] = sel.xpath(query_og('description')).extract_first()
    res['og_image'] = sel.xpath(query_og('image')).extract_first()
    return res

def main(host='127.0.0.1', port=6379, db=0):
    pool = redis.ConnectionPool(host=host, port=port, db=db)
    r = redis.Redis(connection_pool=pool)
    while True:
        try:
            key, value = r.brpop('sb:queue')
            msg = json.loads(value.decode('utf-8'))
            bookmark_id = msg['id']
            res_fetch = fetch(msg['url'])
            res = parse(res_fetch['canonical_url'])
            logging.debug(res_fetch)
            logging.debug(res)
            data = res
            data['bookmark_id'] = bookmark_id
            data['redirected_url'] = res_fetch['redirected_url']
            data['canonical_url'] = res_fetch['canonical_url']
            data['parsed_at'] = datetime.now().isoformat()
            r.lpush('sb:result', json.dumps(data))
        except KeyboardInterrupt:
            logging.exception("Exception message:")
            break
        except:
            logging.exception("Exception message:")
            time.sleep(1)

def test():
    urls = [
        'http://www.pressian.com/news/article.html?no=171574',
        'https://www.theguardian.com/world/2017/oct/07/new-zealand-election-full-results-labour-boost-and-first-refugee-mp',
        'https://www.nytimes.com/2017/10/06/us/harvey-weinstein-sexual-harassment.html?hp&action=click&pgtype=Homepage&clickSource=story-heading&module=photo-spot-region&region=top-news&WT.nav=top-news'
    ]
    for url in urls:
        res_fetch = fetch(url)
        res = parse(res_fetch['canonical_url'])
        logging.debug(urls)
        logging.debug(res)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='Host', default='127.0.0.1')
    parser.add_argument('--port', help='Port', type=int, default=6379)
    parser.add_argument('--db', help='Redis DB', type=int, default=0)
    args = parser.parse_args()
    main(args.host, args.port, args.db)
